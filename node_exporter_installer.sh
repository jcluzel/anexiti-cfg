#!/bin/sh

USER="node_exporter"
FILE="https://github.com/prometheus/node_exporter/releases/download/v1.2.0/node_exporter-1.2.0.linux-amd64.tar.gz"
DIR="node_exporter-1.2.0.linux-amd64"
BIN="node_exporter"
SVC="node_exporter"

useradd --no-create-home $USER
wget -c $FILE -O - | tar -xz
mv $DIR/$BIN /usr/local/bin/
rm -rf $DIR
wget -O /etc/systemd/system/$SVC.service "https://gitlab.com/jcluzel/anexiti-cfg/-/raw/main/$SVC.service"
systemctl start $SVC
systemctl enable $SVC
