#!/bin/sh

EFS="172.20.50.252:/anex-front"
OPT="nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport"

ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
#amazon-linux-extras install -y php8.0
amazon-linux-extras install -y php7.4
yum install -y httpd php-gd php-intl php-mbstring php-opcache php-xml
#yum install -y mod_ssl
#amazon-linux-extras install -y nginx1
systemctl reload php-fpm
echo -e \
"$EFS/conf.d /etc/httpd/conf.d nfs4 $OPT 0 2\n"\
"$EFS/html /var/www/html nfs4 $OPT 0 2" >> /etc/fstab
#"$EFS/certs /etc/pki/tls/certs nfs4 $OPT 0 2\n"\
#"$EFS/private /etc/pki/tls/private nfs4 $OPT 0 2\n"\
mount -a
usermod -a -G apache ec2-user
systemctl start httpd
systemctl enable httpd
